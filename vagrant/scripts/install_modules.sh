dir='/vagrant/modules/'

puppet module install example42/yum --modulepath $dir --version 2.1.25;
puppet module install example42/php  --modulepath $dir --version 2.0.25;
puppet module install puppetlabs-apache --modulepath $dir --version 1.6.0;
puppet module install puppetlabs/mysql --modulepath $dir --version 3.6.1;
puppet module install example42/memcached --modulepath $dir --version 3.0.5;
puppet module install crayfishx/firewalld --modulepath $dir --version 1.2.3;
puppet module install example42-timezone --modulepath $dir --version 2.0.13;
puppet module install example42-ntp --modulepath $dir --version 2.0.16;

yum install -y git
#git clone git@bitbucket.org:boixa2/puppetready.git /vagrant/modules/puppetReady
#git clone https://boixa2@bitbucket.org/boixa2/puppetready.git /vagrant/modules/puppetReady
git clone https://bitbucket.org/boixa2/puppetready.git /vagrant/modules/puppetready

exit 0;
